# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=spice
pkgver=0.15.0
pkgrel=0
pkgdesc="Implements the SPICE protocol"
url="http://www.spice-space.org/"
arch="all"
license="LGPL-2.0-or-later"
makedepends="
	meson
	spice-protocol
	glib-dev
	gdk-pixbuf-dev
	pixman-dev
	openssl-dev
	libjpeg-turbo-dev
	zlib-dev
	opus-dev
	gstreamer-dev
	gst-plugins-base-dev
	orc-dev
	lz4-dev
	cyrus-sasl-dev
	py3-six
	py3-parsing
	"
subpackages="$pkgname-dev $pkgname-server"
source="https://www.spice-space.org/download/releases/spice-server/spice-$pkgver.tar.bz2
	failing-tests.patch
	fix-build.patch
	"

# secfixes:
#   0.15.0-r0:
#     - CVE-2020-14355
#   0.14.3-r1:
#     - CVE-2021-20201
#   0.14.1-r4:
#     - CVE-2019-3813
#   0.14.1-r0:
#     - CVE-2018-10873
#   0.12.8-r4:
#     - CVE-2017-7506
#   0.12.8-r3:
#     - CVE-2016-9577
#     - CVE-2016-9578

build() {
	abuild-meson \
		-Dgstreamer=1.0 \
		-Dlz4=true \
		-Dsasl=true \
		-Dopus=enabled \
		-Dsmartcard=disabled \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

server() {
	pkgdesc="Server library for SPICE"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*server.so.* "$subpkgdir"/usr/lib/
}

sha512sums="
0a776d191c395ce1f7ebbbac47956a00a2765327d3127aeca6e232bd56fd4ccd28750ae1599eb6eb2909ac909cda517d5511faa631166db16b8b75bd4e7b86d9  spice-0.15.0.tar.bz2
28d1b959f6faeccf22e1bbdf61bd9c0e08695c9204fa2d06e2f8173e7a21c60a3485b5af903c16b921ef3f1f0ab410c151b3394b93ed03c1d515870f10253d30  failing-tests.patch
b61ae910c08e26c7788682f6b5df8190c9db7802858f9ca05093bb1bafd226adc2f16382721e4f63cbc09b7196f9ac967d0175c1e16253028e94fa27372ab8e6  fix-build.patch
"
